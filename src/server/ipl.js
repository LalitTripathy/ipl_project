
// Number of matches played per year for all the years in IPL.

function getMatchPerYear(matches){

    let matchesPerYear = {};
    
    for(let i = 0;i<matches.length;i++){
    
        let season = matches[i]["season"];
        
        if(!matchesPerYear.hasOwnProperty(season)){
            
            matchesPerYear[season] = 1;
    
        }else{
            
            matchesPerYear[season]++;
         }
    }
    return matchesPerYear;
}




//This function is used to count the number of matches won per team per year in IPL.

function matchesWonPerTeamPerYear(matches) {

    let matchesWonPerTeam = matches.reduce(function (matchesWonPerTeam, value) {

        if (matchesWonPerTeam.hasOwnProperty(value.season)) {

            if (matchesWonPerTeam[value.season].hasOwnProperty(value.winner)) {
                matchesWonPerTeam[value.season][value.winner]++;
            }

            else {
                matchesWonPerTeam[value.season][value.winner] = 1;
            }
        }

        else {
            matchesWonPerTeam[value.season] = {};
        }

        return matchesWonPerTeam;

    }, {})

    return matchesWonPerTeam;
}
//This function is used to count the extra runs conceded per team in the year 2016.

function extraRunsConcededPerTeam(matches, deliveries,year) {

    let matchId = matches.filter((val) => val.season == year).map((ids) => parseInt(ids.id));
    let extraRuns = deliveries.reduce(function (extraRuns, value) {
        delieveryId = parseInt(value.match_id);

        if (matchId.includes(delieveryId)) {

            if (!extraRuns.hasOwnProperty(value.bowling_team)) {
                extraRuns[value.bowling_team] = parseInt(value.extra_runs);
            }

            else {
                extraRuns[value.bowling_team] += parseInt(value.extra_runs);
            }

        }
        return extraRuns;

    }, {})

    return extraRuns;

}

function topTenEconomicalBowlers(matches, deliveries,year) {
    let matchId = matches.filter((value) => value.season == year).map((ids) => parseInt(ids.id));

    let bowlerStat = deliveries.reduce(function (bowlerStat, value) {
        if (value.is_super_over == 0) {
            let totalRuns = parseInt(value.wide_runs) + parseInt(value.noball_runs) + parseInt(value.batsman_runs);
            let delieveryId = parseInt(value.match_id);

            if (matchId.includes(delieveryId)) {

                if (!bowlerStat.hasOwnProperty(value.bowler)) {

                    if (value.wide_runs > 0 || value.noball_runs > 0) {
                        bowlerStat[value.bowler] = { 'runs': totalRuns, 'balls': 0 };
                    }

                    else {
                        bowlerStat[value.bowler] = { 'runs': totalRuns, 'balls': 1 };
                    }
                }

                else {

                    if (value.wide_runs > 0 || value.noball_runs > 0) {
                        bowlerStat[value.bowler].runs = (bowlerStat[value.bowler].runs + totalRuns);
                    }

                    else {
                        bowlerStat[value.bowler].runs = (bowlerStat[value.bowler].runs + totalRuns);
                        bowlerStat[value.bowler].balls++;
                    }
                }
            }
        }

        return bowlerStat;
    }, {})
    let topTenEconomicalBowlers1 = Object.fromEntries(Object.entries(bowlerStat).map(x => ([x[0], Number(((x[1].runs / (x[1].balls / 6))).toFixed(2))])).sort((x, y) => x[1] - y[1]).filter((x, y) => y < 10));

    return topTenEconomicalBowlers1;
}





module.exports = { 
    getMatchPerYear,
    matchesWonPerTeamPerYear,
    extraRunsConcededPerTeam,
    topTenEconomicalBowlers
};
