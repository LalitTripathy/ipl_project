const converter = require("convert-csv-to-json");
const fs = require("fs");
const path = require("path");
let ipl = require("./ipl");

const matches_path = path.resolve(__dirname,"../data/matches.csv")
const deliveries_path = path.resolve(__dirname,"../data/deliveries.csv")

let deliveries = converter.fieldDelimiter(',').getJsonFromCsv(deliveries_path);


let matches = converter.fieldDelimiter(',').getJsonFromCsv(matches_path);


let matchesPerYear = ipl.getMatchPerYear(matches);
console.log(matchesPerYear);

fs.writeFile ('./../public/output/matchesPerYear.json', JSON.stringify(matchesPerYear), (err, data) => {
    if (err) {
        console.log(err);
    }else {
        console.log("Successfully Created the file");
    }
})

const matchWonPerTeamPerYear = ipl.matchesWonPerTeamPerYear(matches);
console.log(matchWonPerTeamPerYear);

fs.writeFile ('./../public/output/matchWonPerTeamPerYear.json', JSON.stringify(matchWonPerTeamPerYear), (err, data) => {
    if (err) {
        console.log(err);
    }else {
        console.log("Successfully Created the file");
    }
})

const extraRunsConcededPerTeam = ipl.extraRunsConcededPerTeam(matches,deliveries);
console.log(extraRunsConcededPerTeam);


fs.writeFile ('./../public/output/extraRunsConcededPerTeam.json', JSON.stringify(extraRunsConcededPerTeam), (err, data) => {
    if (err) {
        console.log(err);
    }else {
        console.log("Successfully Created the file");
    }
})

const topTenEconomicalBowlers = ipl.topTenEconomicalBowlers(matches,deliveries);


fs.writeFile ('./../public/output/topTenEconomicalBowlers.json', JSON.stringify(topTenEconomicalBowlers), (err, data) => {
    if (err) {
        console.log(err);
    }else {
        console.log("Successfully Created the file");
    }
})